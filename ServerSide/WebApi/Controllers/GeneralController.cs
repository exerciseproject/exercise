﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class GeneralController : ApiController
    {
        public static List<Person> Names=new List<Person>
        {
            new Person() {FirstName="Chaya",LastName="Ostry" },
            new Person() {FirstName="David",LastName="Levi" }
        };

        [HttpGet]
        public IHttpActionResult GetListOfPerson()
        {           
            try
            {               
                return Ok(Names);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }   
    }
}
